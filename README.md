## 概要

Rustでwasmパッケージつくってゲームができた

[デモ](https://static-app.morifuji-is.ninja/snake/)

[ブログ](https://blog.morifuji-is.ninja/post/2020-02-01/)

## Quick start

```sh
$ sh ./build_and_serve.sh
```

## 参考

- https://medium.com/@geekrodion/snake-game-with-rust-javascript-and-webassembly-5e22b357ec7b
- https://rustwasm.github.io/wasm-pack/book/tutorials/npm-browser-packages/template-deep-dive/src-utils-rs.html
