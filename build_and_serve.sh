wasm-pack build
cd www
rm -rf ./node_modules/wasm-snake-game
yarn install --check-files
yarn build
yarn start --host 0.0.0.0 --disable-host-check
