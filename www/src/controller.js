import { Movement } from "wasm-snake-game";

const MOVEMENT_KEYS = {
  [Movement.TOP]: [87, 38],
  [Movement.RIGHT]: [68, 39],
  [Movement.DOWN]: [83, 40],
  [Movement.LEFT]: [65, 37]
}

const STOP_KEY = 32

let xDown = null;
let yDown = null;

export class Controller {
  constructor(onStopAndResume = () => { }) {

    /**
     * PC
     */
    window.addEventListener('keydown', ({ which }) => {
      this.movement = Object.keys(MOVEMENT_KEYS).find(key => MOVEMENT_KEYS[key].includes(which))
    })
    window.addEventListener('keyup', ({ which }) => {
      if (which === STOP_KEY) {
        onStopAndResume()
      }
    })

    /**
     * iPhone
     */
    document.addEventListener('touchstart', (evt) => {
      const firstTouch = evt.touches[0];
      xDown = firstTouch.clientX;
      yDown = firstTouch.clientY;
    }, false);
    document.addEventListener('touchmove', (evt) => {
      if (!xDown || !yDown) {
        return;
      }

      var xUp = evt.touches[0].clientX;
      var yUp = evt.touches[0].clientY;

      var xDiff = xDown - xUp;
      var yDiff = yDown - yUp;

      if (Math.abs(xDiff) > Math.abs(yDiff)) {/*most significant*/
        if (xDiff > 0) {
          this.movement = Movement.LEFT
        } else {
          this.movement = Movement.RIGHT
        }
      } else {
        if (yDiff > 0) {
          this.movement = Movement.TOP
        } else {
          this.movement = Movement.DOWN
        }
      }
    }, false);
  }
}