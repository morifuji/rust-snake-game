
import { Game, Vector } from 'wasm-snake-game'
import { Controller } from './controller'

import CONFIG from './config'
import { View } from "./view.js"

import Storage from "./storage"

export class GameManager {
  constructor() {
    this.stopTime = null
    this.restart()
    this.view = new View(
      this.game.width,
      this.game.height,
      this.render.bind(this)
    )

    this.controller = new Controller(
      this.onStopAndResume.bind(this)
    )
  }

  restart() {
    this.game = new Game(
      CONFIG.WIDTH,
      CONFIG.HEIGHT,
      CONFIG.SPEED,
      CONFIG.SNAKE_LENGTH,
      new Vector(
        CONFIG.SNAKE_DIRECTION_X,
        CONFIG.SNAKE_DIRECTION_Y
      )
    )
    this.lastUpdate = undefined
    this.stopTime = undefined
  }

  render() {
    this.view.render(
      this.game.food,
      this.game.get_snake(),
      this.game.score,
      Storage.getBestScore()
    )
  }

  onStopAndResume() {
    this.stopTime = this.stopTime === null ? new Date() : null
  }

  run() {
    setInterval(this.tick.bind(this), 1000 / CONFIG.FPS)
  }

  tick() {
    const lastUpdate = Date.now()
    if (!this.stopTime) {
      if (this.lastUpdate) {
        this.game.process(lastUpdate - this.lastUpdate, this.controller.movement)

        if (this.game.is_over()) {
          this.restart()
          return
        }

        if (this.game.score > Storage.getBestScore()) {
          localStorage.setItem('bestScore', this.game.score)
          Storage.setBestScore(this.game.score)
        }
      }
      this.render()
    }
    this.lastUpdate = lastUpdate
  }
}